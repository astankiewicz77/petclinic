trigger Trigger_Animal on Animal__c (before insert,after insert) {

    if(Trigger.isBefore && Trigger.isInsert){

        Handler_AnimalTrigger.processBeforeInsertAnimals( Trigger.new );

    }
    if( Trigger.isAfter && Trigger.isInsert ){

        Handler_AnimalTrigger.processAfterInsertAnimals( Trigger.newMap );

    }

}