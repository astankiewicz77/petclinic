public without sharing class Service_HTTPManager {

    @future(callout=true)
    public static void performRemoteCall( Set<Id> animalIds ){

        Map<String,String> apiResponses = new Map<String,String>();
        List<Animal__c> animals = [
                                    SELECT Id,Name,Breed_Description__c 
                                    FROM Animal__c 
                                    WHERE Id 
                                    IN :animalIds
                                ];
        Map<String,API_Service__mdt> cmIntegration = getCMMap();

        for( Animal__c animal : animals ){
            animal = getRemoteDataFromWolframAPI(animal,cmIntegration.get(GlobalConstans.CM_WOLFRAM));
            animal = getRemoteDataFromBingAPI(animal,cmIntegration.get(GlobalConstans.CM_BING));
        }

        update animals;

    }

    private static Map<String,API_Service__mdt> getCMMap(){

        Map<String,API_Service__mdt> customMetadata = new Map<String,API_Service__mdt>();

        List<API_Service__mdt> apiServices = [
                                            SELECT Endpoint__c,HandshakeKey__c,Label,Type__c 
                                            FROM API_Service__mdt 
                                            WHERE Label 
                                            IN :GlobalConstans.CM_INTEGRATION_VALUES
                                            ];

        for(API_Service__mdt element : apiServices){
            customMetadata.put( element.Label , element );
        }

        return customMetadata;
    }

    public static Animal__c getRemoteDataFromWolframAPI( Animal__c animal  , API_Service__mdt apiService ){

        Map<String,String> integrationValues = new Map<String,String>();

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint( apiService.Endpoint__c + animal.Breed_Description__c + apiService.HandshakeKey__c);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {

            // factory pattern
            Interface_GenericResponse wolframXmlResponse = XMLResponseFactory.getProperParser(apiService.Label);
            integrationValues = wolframXmlResponse.parseResponseDOM( response );
            integrationValues.put('Payload',response.getBody());
        }
 
        animal.Description__c = integrationValues.get('Description0');
        animal.Properties__c = integrationValues.get('Property0');

        return animal;

    }

    public static Animal__c getRemoteDataFromBingAPI( Animal__c animal , API_Service__mdt apiService ){

        Map<String,String> integrationValues = new Map<String,String>();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');

        request.setEndpoint( apiService.Endpoint__c + prepareParamsBeforeCall(animal.Breed_Description__c) + '&count=1' );
        request.setHeader( GlobalConstans.INTEGRATION_BING_HEADER , apiService.HandshakeKey__c );
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            
            Interface_GenericResponse bingJSONResponse = XMLResponseFactory.getProperParser(apiService.Label);
            integrationValues = bingJSONResponse.parseResponseDOM( response );

            animal.ImageUrl__c = integrationValues.get('Image');

        }

        return animal;

    }

    public static String prepareParamsBeforeCall(String params){
        // it replaces space with its ASCII symbol in order to process properly on API side
        return params.replace(' ', '%20');
    }

}