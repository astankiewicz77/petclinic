public interface Interface_GenericXMLResponse {

    Map<String,String> parseResponseDOM( HttpResponse response );

}
