public class Handler_AnimalTrigger {

    public static void processBeforeInsertAnimals( List<Animal__c> animals ){

        for( Animal__c animal : animals ){
            animal.Name = Servive_Animal.capitalizeName(animal.Name);
        }

    }

    public static void processAfterInsertAnimals( Map<Id,Animal__c> animals ){
        //todo limit class
        system.debug('@@@@ hello from processAfterInsertAnimals');

        // Queueable_RestService2 qrs2 = new Queueable_RestService2( animals );
        // String jobId = System.enqueueJob( qrs2 );

        // for( Animal__c animal : animals ){
        //     Queueable_RestService restSer = new Queueable_RestService(animal);
        //     String jobId = System.enqueueJob( new Queueable_RestService(animal) );

        // }
        Service_HTTPManager.performRemoteCall( animals.keySet() );

    }

}