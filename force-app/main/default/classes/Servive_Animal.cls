public without sharing class Servive_Animal {


    public static String capitalizeName( String name){
        List<String> names = name.split(' ');
        String namesCapitalized = '';

        for(String nameEl : names){
            namesCapitalized += nameEL.capitalize() + ' ';
        }

        return namesCapitalized;
    }

}
