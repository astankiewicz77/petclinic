global class BatchScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        AssignNewAnimalsToMainDoctor batch = new AssignNewAnimalsToMainDoctor();
        database.executebatch(batch);
    }
}