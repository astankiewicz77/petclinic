public class GlobalConstans {
    
    // INTEGRATION
    public static final String CM_BING = 'MicrosoftBing';
    public static final String CM_WOLFRAM = 'WolframAlpha';
    public static final Set<String> CM_INTEGRATION_VALUES = new Set<String>{CM_BING,CM_WOLFRAM};

    public static final String INTEGRATION_BING_HEADER = 'Ocp-Apim-Subscription-Key';

    //BATCH
    public static final String ANIMALSTATUSNEW = 'New';
    public static final String ANIMALSTATUSASSIGNED = 'Assigned';
    public static final String USERSPECIALIZATIONNOVET = 'No Vet';
    public static final String CAT = 'Cat';
    public static final String DOG = 'Dog';
    public static final String CATS = 'Cats';
    public static final String DOGS = 'Dogs';
    public static final String SMALLANIMALS = 'Small animals';



}