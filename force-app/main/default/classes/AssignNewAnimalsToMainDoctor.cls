global with sharing class AssignNewAnimalsToMainDoctor implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT ID,Name,Status__c,Type__c,Main_Doctor__c FROM Animal__c '
        + 'WHERE Status__c = \''+ GlobalConstans.ANIMALSTATUSNEW +'\'');
    }
    
    
    global void execute(Database.BatchableContext bc, List<Animal__c> scope){
        
        List<User> vets = [SELECT Id, Name, Specialization__c FROM User WHERE Specialization__c != :GlobalConstans.USERSPECIALIZATIONNOVET AND Specialization__c != null AND isActive = true];

        List<User> dogVets = new List<User>();
        List<User> catVets = new List<User>();
        List<User> smallAnimalVets = new List<User>();

        for(User vet : vets){
            if(GlobalConstans.DOGS.equalsIgnoreCase(vet.Specialization__c)){
                dogVets.add(vet);
            }else if(GlobalConstans.CATS.equalsIgnoreCase(vet.Specialization__c)){
                catVets.add(vet);
            }else{
                smallAnimalVets.add(vet);
            }
        }

        for(Animal__c pet : scope){
            
            if(GlobalConstans.DOG.equalsIgnoreCase(pet.Type__c)){
                assignRandomVet(pet, dogVets);
                pet.Status__c = GlobalConstans.ANIMALSTATUSASSIGNED;
            }else if( GlobalConstans.CAT.equalsIgnoreCase(pet.Type__c)){
                assignRandomVet(pet, catVets);
                pet.Status__c = GlobalConstans.ANIMALSTATUSASSIGNED;
            }else{
                assignRandomVet(pet, smallAnimalVets);
                pet.Status__c = GlobalConstans.ANIMALSTATUSASSIGNED;
            }
        }
        
        update scope;
    }
    
    global void finish(Database.BatchableContext bc){
        
    }

    private void assignRandomVet(Animal__c inAnimal, List<User> inVetList ){
        Integer index = (Math.random() * (inVetList.size() - 1)).intValue();
        inAnimal.Main_Doctor__c = inVetList.get(index).Id;
    }
    
}