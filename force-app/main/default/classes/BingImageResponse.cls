public class BingImageResponse {

	public class InsightsMetadata {
		public Integer pagesIncludingCount {get;set;} 
		public Integer availableSizesCount {get;set;} 

		public InsightsMetadata(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'pagesIncludingCount') {
							pagesIncludingCount = parser.getIntegerValue();
						} else if (text == 'availableSizesCount') {
							availableSizesCount = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'InsightsMetadata consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public String x_type {get;set;} // in json: _type
	public Instrumentation instrumentation {get;set;} 
	public String readLink {get;set;} 
	public String webSearchUrl {get;set;} 
	public QueryContext queryContext {get;set;} 
	public Integer totalEstimatedMatches {get;set;} 
	public Integer nextOffset {get;set;} 
	public Integer currentOffset {get;set;} 
	public List<Value> value {get;set;} 

	public BingImageResponse(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == '_type') {
						x_type = parser.getText();
					} else if (text == 'instrumentation') {
						instrumentation = new Instrumentation(parser);
					} else if (text == 'readLink') {
						readLink = parser.getText();
					} else if (text == 'webSearchUrl') {
						webSearchUrl = parser.getText();
					} else if (text == 'queryContext') {
						queryContext = new QueryContext(parser);
					} else if (text == 'totalEstimatedMatches') {
						totalEstimatedMatches = parser.getIntegerValue();
					} else if (text == 'nextOffset') {
						nextOffset = parser.getIntegerValue();
					} else if (text == 'currentOffset') {
						currentOffset = parser.getIntegerValue();
					} else if (text == 'value') {
						value = arrayOfValue(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'BingImageResponse consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Value {
		public String webSearchUrl {get;set;} 
		public String name {get;set;} 
		public String thumbnailUrl {get;set;} 
		public String datePublished {get;set;} 
		public Boolean isFamilyFriendly {get;set;} 
		public String contentUrl {get;set;} 
		public String hostPageUrl {get;set;} 
		public String contentSize {get;set;} 
		public String encodingFormat {get;set;} 
		public String hostPageDisplayUrl {get;set;} 
		public Integer width {get;set;} 
		public Integer height {get;set;} 
		public String hostPageFavIconUrl {get;set;} 
		public Thumbnail thumbnail {get;set;} 
		public String imageInsightsToken {get;set;} 
		public InsightsMetadata insightsMetadata {get;set;} 
		public String imageId {get;set;} 
		public String accentColor {get;set;} 

		public Value(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'webSearchUrl') {
							webSearchUrl = parser.getText();
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'thumbnailUrl') {
							thumbnailUrl = parser.getText();
						} else if (text == 'datePublished') {
							datePublished = parser.getText();
						} else if (text == 'isFamilyFriendly') {
							isFamilyFriendly = parser.getBooleanValue();
						} else if (text == 'contentUrl') {
							contentUrl = parser.getText();
						} else if (text == 'hostPageUrl') {
							hostPageUrl = parser.getText();
						} else if (text == 'contentSize') {
							contentSize = parser.getText();
						} else if (text == 'encodingFormat') {
							encodingFormat = parser.getText();
						} else if (text == 'hostPageDisplayUrl') {
							hostPageDisplayUrl = parser.getText();
						} else if (text == 'width') {
							width = parser.getIntegerValue();
						} else if (text == 'height') {
							height = parser.getIntegerValue();
						} else if (text == 'hostPageFavIconUrl') {
							hostPageFavIconUrl = parser.getText();
						} else if (text == 'thumbnail') {
							thumbnail = new Thumbnail(parser);
						} else if (text == 'imageInsightsToken') {
							imageInsightsToken = parser.getText();
						} else if (text == 'insightsMetadata') {
							insightsMetadata = new InsightsMetadata(parser);
						} else if (text == 'imageId') {
							imageId = parser.getText();
						} else if (text == 'accentColor') {
							accentColor = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Value consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Instrumentation {
		public String x_type {get;set;} // in json: _type

		public Instrumentation(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == '_type') {
							x_type = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Instrumentation consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class QueryContext {
		public String originalQuery {get;set;} 
		public String alterationDisplayQuery {get;set;} 
		public String alterationOverrideQuery {get;set;} 
		public String alterationMethod {get;set;} 
		public String alterationType {get;set;} 

		public QueryContext(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'originalQuery') {
							originalQuery = parser.getText();
						} else if (text == 'alterationDisplayQuery') {
							alterationDisplayQuery = parser.getText();
						} else if (text == 'alterationOverrideQuery') {
							alterationOverrideQuery = parser.getText();
						} else if (text == 'alterationMethod') {
							alterationMethod = parser.getText();
						} else if (text == 'alterationType') {
							alterationType = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'QueryContext consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Thumbnail {
		public Integer width {get;set;} 
		public Integer height {get;set;} 

		public Thumbnail(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'width') {
							width = parser.getIntegerValue();
						} else if (text == 'height') {
							height = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Thumbnail consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static BingImageResponse parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new BingImageResponse(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	








    private static List<Value> arrayOfValue(System.JSONParser p) {
        List<Value> res = new List<Value>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Value(p));
        }
        return res;
    }



}