public class BingJSONResponse implements Interface_GenericResponse {
    
    public Map<String,String> parseResponseDOM( HttpResponse response ){

        Map<String,String> responseMatrix = new Map<String,String>();


        BingImageResponse bingImageResponseObject = (BingImageResponse)JSON.deserialize(response.getBody(), BingImageResponse.class);
        responseMatrix.put( 'Image' , bingImageResponseObject.value[0].contentUrl );

        return responseMatrix;
    }

}