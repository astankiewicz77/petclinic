public with sharing class XMLResponseFactory {
    
    private String factoryType;
    
    public XMLResponseFactory( String factoryType ) {
        this.factoryType = factoryType;
    }

    public static Interface_GenericResponse getProperParser( String parserType ){

        Interface_GenericResponse factory = null;

        if( parserType!= null && parserType.equalsIgnoreCase( GlobalConstans.CM_WOLFRAM ) ){
            factory = new WolframXMLResponse();
        }
        else if(parserType!= null && parserType.equalsIgnoreCase( GlobalConstans.CM_BING )){
            factory = new BingJSONResponse();
        }

        return factory;

    }

}