public interface Interface_GenericResponse {

    Map<String,String> parseResponseDOM( HttpResponse response );

}