public class WolframXMLResponse implements Interface_GenericResponse {

    public Map<String,String> parseResponseDOM( HttpResponse response ){

        String responseInformation = '';
        Map<String,String> responseMatrix = new Map<String,String>();

        Dom.Document document = response.getBodyDocument();

        Dom.XmlNode queryResult = document.getRootElement();

        List<Dom.XmlNode> nodes = queryResult.getChildElements();

        for(Dom.XmlNode node : nodes){

            if(node.getAttribute('title',null) != null && node.getAttribute('title',null).containsIgnoreCase('description')) {
                Integer nodeCounter = 0;
                for(Dom.XmlNode childNode : node.getChildElements()){
                    if(childNode.getName() != null && childNode.getName().containsIgnoreCase('subpod')) {
                        for(Dom.XmlNode childNode2 : childNode.getChildElements()){
                            if(childNode2.getName() != null && childNode2.getName().containsIgnoreCase('plaintext')){
                                responseMatrix.put('Description'+nodeCounter,childNode2.getText());
                                system.debug(LoggingLevel.INFO,'@@ description: ' + childNode2.getText() );
                                nodeCounter++;
                            }
                        }
                    }

                }
            }
            if(node.getAttribute('title',null) != null && node.getAttribute('title',null).containsIgnoreCase('properties')){
                Integer nodeCounter = 0;
                for(Dom.XmlNode childNode : node.getChildElements()){

                    if(childNode.getName() != null && childNode.getName().containsIgnoreCase('subpod')) {
                        for(Dom.XmlNode childNode2 : childNode.getChildElements()){
                            if(childNode2.getName() != null && childNode2.getName().containsIgnoreCase('plaintext')){
                                responseMatrix.put('Property' + nodeCounter , childNode2.getText());
                                system.debug(LoggingLevel.INFO,'@@ properties: ' + childNode2.getText() );

                                nodeCounter++;
                            }
                        }
                    }
                }

            }

        }

        return responseMatrix;
    }

}